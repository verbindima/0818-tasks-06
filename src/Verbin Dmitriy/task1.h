#define N_PI       3.14159265358979323846
#include <math.h>
class Circle
{
private:
	double Radius, Ference, Area;
public:
	Circle()
	{
		Radius = NULL;
		Ference = NULL;
		Area = NULL;
	}

	void setRadius(double radius)
	{
		if (radius > 0)
		{
			Radius = radius;
			Ference = 2 * radius * N_PI; 
			Area = N_PI * radius * radius;
		}
	}

	void setFerence(double ference)
	{
		if (ference > 0)
		{
			Radius = ference / (2 * N_PI);
			Ference = ference; 
			Area = N_PI * Radius * Radius;
		}
	}

	void setArea(double area)
	{
		if (area > 0)
	{
			Radius = sqrt(area / N_PI);
			Ference = 2 * Radius * N_PI; 
			Area = area;
		}
	}

	double getRadius()
	{
		return Radius;
	}

	double getFerence()
	{
		return Ference;
	}

	double getArea()
	{
		return Area;
	}

};